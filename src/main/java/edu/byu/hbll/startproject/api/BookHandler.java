package edu.byu.hbll.startproject.api;

import java.io.IOException;
import java.util.Iterator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("book")
public class BookHandler {
	private static Client client = ClientBuilder.newClient();
	private ObjectMapper mapper = new ObjectMapper();
	private final String linkFirst = "https://openlibrary.org/api/books?bibkeys=ISBN:";
	private final String linkLast = "&jscmd=data&format=json";

	// method for using a tree model
	@GET
	@Path("{isbn}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBookTree(@PathParam("isbn") String isbn) {
		String response = "";
		JsonNode jsonResponse;
		try {
			response = client.target(linkFirst + isbn + linkLast).request().get(String.class);
			jsonResponse = mapper.readTree(response);
		} catch (IOException e) {
			e.printStackTrace();
			return "IO error";
		}
		JsonNode bookNode = jsonResponse.get("ISBN:" + isbn);
		if (bookNode == null) {
			return "ISBN not found";
		}
		ObjectNode returnNode = mapper.createObjectNode();

		returnNode.put("title", bookNode.get("title").textValue());
		returnNode.put("url", bookNode.get("url").textValue());

		// we use an array of objects for the next one, since a book can have multiple
		// authors
		JsonNode retAuthorsNode = returnNode.putArray("authors");
		JsonNode authorsNode = bookNode.get("authors");
		Iterator<JsonNode> it = authorsNode.elements();
		while (it.hasNext()) {
			JsonNode curNode = it.next();
			JsonNode newAuthor = ((ArrayNode) retAuthorsNode).addObject();
			((ObjectNode) newAuthor).put("url", curNode.get("url").textValue());
			((ObjectNode) newAuthor).put("name", curNode.get("name").textValue());
		}

		return returnNode.toString();
	}

	// method for using a POJO
	@GET
	@Path("pojo/{isbn}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBookPojo(@PathParam("isbn") String isbn) {
		String response = "";
		JsonNode jsonResponse;
		try {
			// the root object doesn't match our POJO; we need to go inside of it
			response = client.target(linkFirst + isbn + linkLast).request().get(String.class);
			jsonResponse = mapper.readTree(response);
		} catch (IOException e) {
			e.printStackTrace();
			return "IO error";
		}
		// This grabs the book object inside the ISBN object
		JsonNode bookNode = jsonResponse.get("ISBN:" + isbn);
		if (bookNode == null) {
			return "ISBN not found";
		}

		ParsedData parsedData;
		try {
			parsedData = mapper.readValue(bookNode.toString(), ParsedData.class);
			response = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(parsedData);
		} catch (IOException e) {
			e.printStackTrace();
			return "Error parsing JSON";
		}

		return response;

	}
}