package edu.byu.hbll.startproject.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class BookHandlerTest {
	private String harrypotter = "{\"title\":\"Harry Potter and the Goblet of Fire\",\"url\":\"https://openlibrary.org/books/OL6800755M/Harry_Potter_and_the_Goblet_of_Fire\",\"authors\":[{\"url\":\"https://openlibrary.org/authors/OL23919A/J._K._Rowling\",\"name\":\"J. K. Rowling\"}]}";
	private String programminglang = "{\"title\":\"Programming languages\",\"url\":\"https://openlibrary.org/books/OL19536355M/Programming_languages\",\"authors\":[{\"url\":\"https://openlibrary.org/authors/OL445464A/Terrence_W._Pratt\",\"name\":\"Terrence W. Pratt\"}]}";
	private String wayofkings = "{\"title\":\"The Way of Kings (The Stormlight Archive #1)\",\"url\":\"https://openlibrary.org/books/OL25438341M/The_Way_of_Kings_(The_Stormlight_Archive_1)\",\"authors\":[{\"url\":\"https://openlibrary.org/authors/OL1394865A/Brandon_Sanderson\",\"name\":\"Brandon Sanderson\"}]}";
	private String corejava = "{\"title\":\"Core Java for the Impatient\",\"url\":\"https://openlibrary.org/books/OL25651290M/Core_Java_for_the_Impatient\",\"authors\":[{\"url\":\"https://openlibrary.org/authors/OL25278A/Cay_S._Horstmann\",\"name\":\"Cay S. Horstmann\"}]}";

	private String harrypotterIsbn = "0439139597";
	private String programminglangIsbn = "0136780121";
	private String wayofkingsIsbn = "0765365278";
	private String corejavaIsbn = "0321996321";

	private BookHandler bookHandler;

	@Test
	public void testGetBookTree() {
		bookHandler = new BookHandler();
		try {
			JSONAssert.assertEquals(harrypotter, bookHandler.getBookTree(harrypotterIsbn), true);
			JSONAssert.assertEquals(programminglang, bookHandler.getBookTree(programminglangIsbn), true);
			JSONAssert.assertEquals(wayofkings, bookHandler.getBookTree(wayofkingsIsbn), true);
			JSONAssert.assertEquals(corejava, bookHandler.getBookTree(corejavaIsbn), true);
		} catch (JSONException e) {
			e.printStackTrace();
			fail("JSONException was thrown");
		}
		assertEquals("ISBN not found", bookHandler.getBookTree("asdf"));
		bookHandler = null;
	}

	@Test
	public void testGetBookPojo() {
		bookHandler = new BookHandler();
		try {
			JSONAssert.assertEquals(harrypotter, bookHandler.getBookPojo(harrypotterIsbn), true);
			JSONAssert.assertEquals(programminglang, bookHandler.getBookPojo(programminglangIsbn), true);
			JSONAssert.assertEquals(wayofkings, bookHandler.getBookPojo(wayofkingsIsbn), true);
			JSONAssert.assertEquals(corejava, bookHandler.getBookPojo(corejavaIsbn), true);
		} catch (JSONException e) {
			e.printStackTrace();
			fail("JSONException was thrown");
		}
		assertEquals("ISBN not found", bookHandler.getBookPojo("asdf"));
		bookHandler = null;
	}
}
